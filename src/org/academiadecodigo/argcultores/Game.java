package org.academiadecodigo.argcultores;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.util.Random;

public class Game extends Object implements KeyboardHandler {

    private Picture background;
    private Picture player;
    private Picture[] beer;
    private int beerArrayLength;
    final static int PADDING = 10;
    public static int countWin;
    public static int countLose;
//    private boolean play;

    public Game() {

        // background
        background = new Picture(PADDING, PADDING, "resources/praia.png");
        background.draw();

        // my image
        player = new Picture(300, 450, "resources/player.png");
        player.draw();


        // play
//        play = true;

        // array of beer
        beerArrayLength = 100;
        this.beer = new Picture[beerArrayLength];

        // keyboard initialization
        keyboardInit();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_RIGHT:

                if (player.getX() < 630) {

                    player.translate(30, 0);
                }
                break;


            case KeyboardEvent.KEY_LEFT:

                if (player.getX() > 50) {

                    player.translate(-30, 0);

                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    public void keyboardInit() {
        Keyboard keyboard = new Keyboard(this);

        KeyboardEvent up = new KeyboardEvent();
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        up.setKey(KeyboardEvent.KEY_UP);
        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        down.setKey(KeyboardEvent.KEY_DOWN);
        keyboard.addEventListener(down);

        KeyboardEvent right = new KeyboardEvent();
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        right.setKey(KeyboardEvent.KEY_RIGHT);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        left.setKey(KeyboardEvent.KEY_LEFT);
        keyboard.addEventListener(left);

        KeyboardEvent space = new KeyboardEvent();
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        space.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(space);
    }


    public void startGame() throws InterruptedException {

        beersGenerator();

        moveBeers();

    }

    public void beersGenerator() {

        for (int i = 0; i < this.beer.length; i++) {
            Random r = new Random();
            int rand = r.nextInt(50,630);

            this.beer[i] = new Picture(rand, 50, "resources/beer.png");
        }
    }

    public void moveBeers() throws InterruptedException {

        int speed = 11;
        Game.countWin = 0;
        Game.countLose = 0;

        for (int i = 0; i < beer.length; i++) {

            beer[i].draw();

            while (beer[i].getY() < 500) {

                if (Game.countWin == 5) {
                    speed = 9; // first speed boost
                }

                if (Game.countWin == 10) {
                    speed = 7; // second speed boost
                }

                if (Game.countWin == 15) {
                    speed = 5; // second speed boost
                }

                Thread.sleep(speed);
                beer[i].translate(0, 2); // move a step-down

                if ((beer[i].getY() == player.getY()) && (Math.abs(beer[i].getX() - player.getX()) < 55)) {

                    beer[i].delete();

                    Game.countWin++;
                    System.out.println("PEGOU " + Game.countWin);

                } else if (beer[i].getY() > 499) {

                    countLose++;
                    System.out.println("PERDEU " + countLose);

                    if (countLose >= 5) {
                        return;
                    }
                }

            }

        }

//        this.play = false;
    }

    public void checkCollision() {


    }

}